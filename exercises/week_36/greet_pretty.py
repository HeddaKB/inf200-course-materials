"""
Generate a pretty greeting.

Ask user to input a name and print a framed greeting including that name.

Hans Ekkehard Plesser / NMBU
"""

name = input("Please enter your name: ")
greeting = f"* Welcome to INF200, {name}! *"
width = len(greeting)

print()
print("*" * width)
print(greeting)
print("*" * width)
