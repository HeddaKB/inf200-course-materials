"""
Example solution for text analysis task.

This code finds the name of friends in each sentence 
of a text with specified format.

This version uses the regex package to fully support
unicode word characters and not only A–Z.

Before running this code, you need to

    conda install regex
"""

__author__ = 'Hans Ekkehard Plesser, NMBU'

import regex as re


def find_friends(statements):
    """
    Finds name of friends in a text using regex.

    Assume that each sentence begins with a name, that each name
    begins with a capital letter, and that each name is at least
    two letters long.

    :param statements: List of statements to parse for friends.
    :return: List of friend-name tuples.
    """

    # The regex contains two groups, one for each name to match.
    #
    # The pattern here uses Unicode character properties to match
    # upper- and lowercase letters as defined by Unicode.
    # A \p{} term represents a Unicode property, \p{L} any character
    # which Unicode considers a letter, and \p{Lu} and \p{Ll} upper-
    # and lowercase letters, respectively.
    #
    # For more about Unicode properties in regexes, see
    # https://www.regular-expressions.info/unicode.html
    #
    # Python's built-in re packages does not support Unicode properties
    # as of Python 3.10, so one needs to use the regex package to work
    # with them.

    friend_regex = re.compile(r'^(\p{Lu}\p{Ll}+)\b.*\b(\p{Lu}\p{Ll}+)\b.*')

    # findall() returns a list of matches. We expect one match per sentence
    # and extract that from the list.
    return [friend_regex.findall(s)[0] for s in statements]


if __name__ == '__main__':
    stmts = ['Ali and Per are friends.',
             'Kari and Joe know each other.',
             'James has known Peter since school days.',
             'Ørjan knows Åse',
             'Søren is a school pal of Käthe.',
             'Maßen knows Johnson.']
    print(f'{"Friendships":^23s}')
    print('-' * 23)
    for a, b in find_friends(stmts):
        print(f'{a:>10s} - {b:10s}')
